import requests
import smtplib  # built-in module for sending email
import os  # used for accessing environment variables in Python
import paramiko  # used to SSH to Linode server
import linode_api4  # used for Linode client
import time
import schedule

# get environment variables (constants, capitalized)  using their names and save as variables
# set environment variable values in PyCharm Editor
EMAIL_ADDRESS = os.environ.get('EMAIL_ADDRESS')
EMAIL_PASSWORD = os.environ.get('EMAIL_PASSWORD')
LINODE_TOKEN = os.environ.get('LINODE_TOKEN')


def restart_server_and_container():
    # restart Linode server
    # LinodeClient needs a parameter for the Linode token for authentication
    # create API token in Linode web portal first
    print('Rebooting the server...')
    client = linode_api4.LinodeClient(LINODE_TOKEN)
    # using client, connect to Linode instance
    # Linode ID is found in Linode web portal
    nginx_server = client.load(linode_api4.Instance, 49185971)
    # reboot server
    nginx_server.reboot()

    # restart the application
    # timing issue when restart docker container after execute reboot, so use while loop
    while True:
        # get status of Linode server over and over by loading instance again
        nginx_server = client.load(linode_api4.Instance, 49185971)
        # wait until server is in running state, only then, restart container
        if nginx_server.status == 'running':
            # add 5 extra seconds of waiting time
            time.sleep(5)
            restart_container()
            break


# create function send_email so don't have to repeat code in multiple places
# pass msg as a parameter because it's the only thing that's different
def send_notification(email_msg):
    print('Sending an email...')
    # send email to me
    # we need a sender and receiver
    # function SMTP configures which email provider and port to use
    # with statement alternative to try/finally for exception handling
    # with statement used with unmanaged, external resources
    with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
        # assign SMTP function to variable called smtp
        # use smtp variable and call starttls function on it
        # starttls for security, encrypt communication from python to email server
        smtp.starttls()
        # identifies python application with mail server on the encrypted connection
        smtp.ehlo()
        # log in python to Two-Step enabled gmail account
        # Get from https://myaccount.google.com/apppasswords
        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        # save Email Subject and Box into a variable
        message = f"Subject: SITE DOWN\n{email_msg}"
        # send an email
        smtp.sendmail(EMAIL_ADDRESS, EMAIL_ADDRESS, message)


# create function to restart the application, since we restart it in two places
def restart_container():
    print('Restarting the application...')
    # restart the application (docker container)
    # connect to Linode server using SSH from Python program
    # execute docker start command
    ssh = paramiko.SSHClient()
    # when connect for first time, get prompt for missing host key and type in Y to confirm
    # automatically add host key to server
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    # open SSH connection
    ssh.connect(hostname='45.79.162.147', username='root', key_filename=r"C:/Users/Joseph/.ssh/id_rsa")
    # stdin is what you type in terminal, stdout is what you get printed back, stderr command results in error
    # execute docker command and set to stdin, stdout, and stderr variables
    stdin, stdout, stderr = ssh.exec_command('docker start 453c5e0ae05d')
    # print stdout and call readlines function to read contents of file
    print(stdout.readlines())
    # close SSH connection
    ssh.close()
    # print('Application restarted')


# what if server does not return a response? Ex. request times out or server down
# in this case, get an exception from requests.get line below
# for testing, login to server and stop docker container
# get ConnectionError exception, if the if/else does not get executed
# therefore, use try/except block
def monitor_application():
    try:
        # make a request/call to our nginx application, same as when we type in browser
        response = requests.get('http://45-79-162-147.ip.linodeusercontent.com:8080/')
        print(response)

        # mirrors same text from browser in HTML format
        # print(response.text)
        # print status code numeric value
        # print(response.status_code)

        # check if application is accessible and gives back status code 200
        # if False: - used for testing the else statement
        if response.status_code == 200:
            print('Application is running successfully!')
        else:
            print('Application down. Fix it!')
            # call send_notification function
            msg = f'Application returned {response.status_code}'
            send_notification(msg)
            # call restart_container function
            restart_container()

    # call Exception "ex"
    # repeating code in multiple places - copy and paste from above to send email
    except Exception as ex:
        print(f'Connection error happened: {ex}')
        msg = "Application not accessible at all."
        send_notification(msg)
        restart_server_and_container()


schedule.every(5).minutes.do(monitor_application)

while True:
    schedule.run_pending()
